﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BooleanExplorer.cs" company="Tekla">
//   Copyright © Tekla 2009-2011
// </copyright>
// <summary>
//   A class which demonstrates how to explore the model, parts, and the boolean operations on parts.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Lesson6
{
    using System;
    using System.Collections.Generic;

    using Tekla.Structures.Model;

    /// <summary>
    /// A class which demonstrates how to explore the model, parts, and the boolean operations on parts.
    /// </summary>
    public static class BooleanExplorer
    {
        /// <summary>
        /// Enumerates through all or selected parts in the model and the boolean operators on each part.
        /// </summary>
        /// <param name="selectedObjectsOnly">
        /// True to enumerate only selected parts, false for all parts in the model.
        /// </param>
        public static void EnumeratePartsAndBooleans(bool selectedObjectsOnly = true)
        {
            var modelObjectEnumerator = GetModelObjectEnumerator<Part>(selectedObjectsOnly);

            foreach (var part in modelObjectEnumerator.GetObjectsByType<Part>())
            {
                // Output the part
                Console.WriteLine("Part: {0}, {1}", part.Profile.ProfileString, part);

                // get the list of boolean operators
                var booleanOperators = part.GetBooleans();

                foreach (var booleanOperator in booleanOperators.GetObjectsByType<Fitting>())
                {
                    // output the fitting
                    Console.WriteLine("\tFitting: O:{0}, X:{1}, Y:{2}", booleanOperator.Plane.Origin, booleanOperator.Plane.AxisX, booleanOperator.Plane.AxisY);
                }

                foreach (var booleanOperator in booleanOperators.GetObjectsByType<CutPlane>())
                {
                    // output the cut plane
                    Console.WriteLine("\tCutPlane: O:{0}, X:{1}, Y:{2}", booleanOperator.Plane.Origin, booleanOperator.Plane.AxisX, booleanOperator.Plane.AxisY);
                }

                foreach (var booleanOperator in booleanOperators.GetObjectsByType<BooleanPart>())
                {
                    switch (booleanOperator.Type)
                    {
                        case BooleanPart.BooleanTypeEnum.BOOLEAN_ADD:
                            // output the part add
                            Console.WriteLine("\tPartAdd: {0}", booleanOperator.OperativePart.Profile.ProfileString);
                            break;
                        case BooleanPart.BooleanTypeEnum.BOOLEAN_CUT:
                            // output the cut part
                            Console.WriteLine("\tPartCut: {0}", booleanOperator.OperativePart.Profile.ProfileString);
                            break;
                    }

                    booleanOperator.OperativePart.GetBooleans();
                }

                foreach (var booleanOperator in booleanOperators.GetObjectsByType<EdgeChamfer>())
                {
                    // output the edge chamfer
                    Console.WriteLine("\tEdgeChamfer: {0}x{1}", booleanOperator.Chamfer.X, booleanOperator.Chamfer.Y);
                }
            }

        }

        /// <summary>
        /// Get the model object enumerator for all or for selected objects
        /// </summary>
        /// <typeparam name="TModelObjectType">
        /// The type of model objects to get if selecting all. This has no effect for selected objects only.
        /// </typeparam>
        /// <param name="selectedObjectsOnly">
        /// The selected objects only.
        /// </param>
        /// <returns>
        /// A model object enumerator
        /// </returns>
        private static ModelObjectEnumerator GetModelObjectEnumerator<TModelObjectType>(bool selectedObjectsOnly) where TModelObjectType : ModelObject
        {
            ModelObjectEnumerator modelObjectEnumerator;
            if (selectedObjectsOnly)
            {
                var modelObjectSelector = new Tekla.Structures.Model.UI.ModelObjectSelector();
                modelObjectEnumerator = modelObjectSelector.GetSelectedObjects();
            }
            else
            {
                modelObjectEnumerator = new Model().GetModelObjectSelector().GetAllObjectsWithType(new[] { typeof(TModelObjectType) });
            }

            return modelObjectEnumerator;
        }

        /// <summary>
        /// Provides strongly typed enumeration over a ModelObjectEnumerator.
        /// </summary>
        /// <param name="modelObjectEnumerator">
        /// The model object enumerator.
        /// </param>
        /// <param name="selectBeforeReturn">
        /// Specifies whether instances should be instantiated when enumerating.
        /// </param>
        /// <typeparam name="TModelObjectType">
        /// Specifies which type of ModelObject to return.
        /// </typeparam>
        /// <returns>
        /// An IEnumerable of the specified object types contained in the ModelObjectEnumerator
        /// </returns>
        private static IEnumerable<TModelObjectType> GetObjectsByType<TModelObjectType>(this ModelObjectEnumerator modelObjectEnumerator, bool selectBeforeReturn = true) where TModelObjectType : ModelObject
        {
            modelObjectEnumerator.Reset();
            modelObjectEnumerator.SelectInstances = false;
            while (modelObjectEnumerator.MoveNext())
            {
                var modelObject = modelObjectEnumerator.Current as TModelObjectType;
                if (modelObject == null)
                {
                    continue;
                }

                if (selectBeforeReturn)
                {
                    modelObject.Select();
                }

                yield return modelObject;
            }
        }
    }
}
