﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
// Tekla Structures Namespaces
using Tekla.Structures;
using Tekla.Structures.Model;
using Tekla.Structures.Geometry3d;
using Tekla.Structures.Model.UI;
using TSM = Tekla.Structures.Model;
using T3D = Tekla.Structures.Geometry3d;
using TSMUI = Tekla.Structures.Model.UI;
using TSO = Tekla.Structures.Model.Operations;


namespace WindowsFormsApplication2
{
    class SerializeData
    {
        Stream stream = null;
        BinaryFormatter bformatter = null;
        String txtFileName = "";
        public TSM.Part deserializedPart = null;
        public SerializeData(String filename)
        {
            txtFileName = filename;
            stream = File.Open(txtFileName, FileMode.Create);
            bformatter = new BinaryFormatter();
        }

        public void SerialiteObject(TSM.Part objectToSerialize)
        {
            bformatter.Serialize(stream, objectToSerialize);
        }

        public void DeserializeObjects()
        {
            TSM.Part objectToDeserialize = null;
            stream = File.Open(txtFileName, FileMode.Open);
            try
            {
                while (stream.CanSeek)
                {
                    objectToDeserialize = (TSM.Part)bformatter.Deserialize(stream);

                    if (objectToDeserialize is TSM.Part)
                    {                      
                        //TSM.Part deserializedPart = (TSM.Part)objectToDeserialize;
                        deserializedPart = (TSM.Part)objectToDeserialize;                     
                    }
                }
            }
            catch (SerializationException ex)
            {
                Console.WriteLine(ex.Message);
                //throw;
            }
        }
        public void closeStream()
        {
            stream.Close();
        }
    }
}
