﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
// Tekla Structures Namespaces
using Tekla.Structures;
using Tekla.Structures.Model;
using Tekla.Structures.Geometry3d;
using Tekla.Structures.Model.UI;
using TSM = Tekla.Structures.Model;
using T3D = Tekla.Structures.Geometry3d;
using TSMUI = Tekla.Structures.Model.UI;
using TSO = Tekla.Structures.Model.Operations;
// Additional Namespace references
using System.IO;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace WindowsFormsApplication2
{
    [Serializable]
    class Class_Level_1
    {
        //public string Name { get; set; }
        //public TSM.Part SelectedPart { get; set; }
        public static SerializeData _SerializeData = new SerializeData("SelectedPart.P2LI");

        public static string SelectPartOnly()
        {
            string report = string.Empty;
            try
            {
                ///////////////////////////////////////////////////////////////////////////////////////
                //      Pick a Part and Save it to File
                ///////////////////////////////////////////////////////////////////////////////////////
                //SerializeData _SerializeData = new SerializeData("SelectedPart.L2P");

                // Object will be saved to this filename:
                Part myPart = PickAPart("Pick the part for intersecting");

                // Start Serializing Data:
                _SerializeData.SerialiteObject(myPart);
                _SerializeData.closeStream();

                // Start Deserializing Data:
                _SerializeData.DeserializeObjects();
                _SerializeData.closeStream();
            }
            catch (Exception ex)
            {
                if (ex.Message != "Error")
                {
                    Console.WriteLine(ex.Message + ex.StackTrace);
                }
            }
            return report;
        }
        //------------------------------------------------------------------------------------
        public static string LineIntersectionWithPart()
        {
            string report = string.Empty;

            try
            {//2.
                ///////////////////////////////////////////////////////////////////////////////////////
                //      Pick a Part and Save it to File
                ///////////////////////////////////////////////////////////////////////////////////////
                //SerializeData _SerializeData = new SerializeData("SelectedPart.P2LI");

                // Object will be saved to this filename:
                Part myPart = PickAPart("Pick the part for intersecting");

                // Start Serializing Data:
                _SerializeData.SerialiteObject(myPart);
                _SerializeData.closeStream();

                // Start Deserializing Data:
                _SerializeData.DeserializeObjects();
                _SerializeData.closeStream();

                // Retrive Deserialized Data to Part:
                TSM.Part deserializedPart = null;
                deserializedPart = _SerializeData.deserializedPart;

                // Copy to Part:
                myPart = deserializedPart;
                ///////////////////////////////////////////////////////////////////////////////////////

                //// Pick Point of Intersection:
                T3D.Point p1 = PickAPoint("Pick the start of the line");

                //T3D.Point p2 = PickAPoint("Pick the end of the line");   
                double x1 = p1.X;
                double y1 = p1.Y;
                double z1 = p1.Z;

                //p1.Y += 30.0f;

                T3D.Point p2 = new T3D.Point();
                //T3D.Point p3 = new T3D.Point();
                //p3.Z = -3000.0f;

                // TODO set +/- distances for other directions (x+, x-, y+, ...):
                //...
                // Just Define p2 to be somewhere in X:
                // IF we select Y- then:    
                p2.Y = 3000.0f;

                BaseForm _BaseForm = new BaseForm();
                Class_Level_2 _Class_Level_2 = new Class_Level_2();




                //// 3. Get the currrent workplane and save it
                var model = new Model();
                var workPlaneHandler = model.GetWorkPlaneHandler();
                var firstWorkplane = workPlaneHandler.GetCurrentTransformationPlane();
                //

                //// 4. Move the Work plane to point:
                Vector Xvector = new Vector(1, 0, 0);
                Vector Yvector = new Vector(0, 1, 0);
                TransformationPlane myPlane = new TransformationPlane(p1, Xvector, Yvector);
                WorkPlaneHandler planeHandler = new Model().GetWorkPlaneHandler();
                planeHandler.SetCurrentTransformationPlane(myPlane);
                //

                //// 5. Move point reverse (Because we moved WP only. Now the point p1 will be located in the same absolute position)
                p1.X = p1.X - x1;
                p1.Y = p1.Y - y1;
                p1.Z = p1.Z - z1;

                //p1.Y += 3000.0f;

                #region Not Used Code

                /*
                Class_Level_1 c1 = new Class_Level_1();
                Class_Level_1 c2 = new Class_Level_1();          
                c1.Name = "aaa";
                c1.SelectedPart = myPart;
                Console.WriteLine(c1.Name);
                */

                // 2.a. Save Part, ready to get from antoher function
                //var _myPart = new Tuple<TSM.Part>(myPart);
                //ObjectToByteArray(myPart);
                //MemoryStream memoryStream = new MemoryStream();
                //Class_Level_1 class_Level_1 = new Class_Level_1();
                //class_Level_1.n1 = 1;
                //class_Level_1.n2 = 20;
                //IFormatter formatter = new BinaryFormatter();
                //Stream stream = new FileStream("MyFile.bin",
                //                         FileMode.Create,
                //                         FileAccess.Write, FileShare.None);
                //formatter.Serialize(stream, class_Level_1);
                //stream.Close();
                /*
                UnicodeEncoding uniEncoding = new UnicodeEncoding();
                int count;
                byte[] byteArray;
                byte[] str1 = uniEncoding.GetBytes("0123456789");
                byte[] str2 = uniEncoding.GetBytes("9876543210");

                using (MemoryStream memStream = new MemoryStream(100))
                {
                    // Write to stream
                    memStream.Write(str1, 0, str1.Length);

                    // Read from stream
                    byteArray = new byte[memStream.Length];
                    count = memStream.Read(byteArray, 0, 20);
                }
                */


                //   TransformationPlane currentPlane = model.GetWorkPlaneHandler().GetCurrentTransformationPlane();
                //   Matrix currentMatrix = currentPlane.TransformationMatrixToGlobal;
                //   T3D.Point origin = currentMatrix.Transform(p1);
                //   Vector axisX = new Vector(currentMatrix.Transform(new T3D.Point(0, 0, 0)) + p1);

                /*
                // Moves Origin to:
                TransformationPlane oldTP = model.GetWorkPlaneHandler().GetCurrentTransformationPlane();
                T3D.Point newPoint = oldTP.TransformationMatrixToLocal.Transform(new T3D.Point(0, 0, 200));
                ModelViewEnumerator Views = ViewHandler.GetSelectedViews();

                while (Views.MoveNext())
                {
                    Tekla.Structures.Model.UI.View View = Views.Current;

                    //if (...) // check that this is the view you want to change

                    {
                        CoordinateSystem CS = View.ViewCoordinateSystem;
                        CS.Origin.X += 200;
                        model.GetWorkPlaneHandler().SetCurrentTransformationPlane(new TransformationPlane(CS));
                        model.CommitChanges();
                    }
                }
                */

                /*
                TSM.Grid grid = new TSM.Grid();
                CoordinateSystem CS = grid.GetCoordinateSystem();
                CS.Origin.Z += 12000;
                model.GetWorkPlaneHandler().SetCurrentTransformationPlane(new TransformationPlane(CS));
                */

                //currentMatrix = currentPlane.TransformationMatrixToLocal;

                /*
                TransformationPlane currentPlane = model.GetWorkPlaneHandler().GetCurrentTransformationPlane();
                Matrix currentMatrix = currentPlane.TransformationMatrixToGlobal;
                T3D.Point origin = currentMatrix.Transform(p1);
                Vector axisX = new Vector(currentMatrix.Transform(new T3D.Point(1000, 0, 0)) - p2);

                Matrix rotateMatrix = MatrixFactory.Rotate(90 * Math.PI / 180, new Vector(1000, 0, 0));
                T3D.Point rotatedYAxis = rotateMatrix.Transform(new T3D.Point(0, 1000, 0));
                Vector axisY = new Vector(currentMatrix.Transform(rotatedYAxis) - origin);

                model.GetWorkPlaneHandler().SetCurrentTransformationPlane(new TransformationPlane());

                TransformationPlane newPlane = new TransformationPlane(origin, axisX, axisY);

                model.GetWorkPlaneHandler().SetCurrentTransformationPlane(newPlane);
                model.CommitChanges();
                */

                //CoordinateSystem cosys = new CoordinateSystem(p1, new Vector(p2.X - p1.X, p2.Y - p1.Y, p2.Z - p1.Z), new Vector(p3.X - p1.X, p3.Y - p1.Y, p3.Z - p1.Z));

                //model.GetWorkPlaneHandler().SetCurrentTransformationPlane(new TransformationPlane(cosys));

                //var secondWorkplane = workPlaneHandler.GetCurrentTransformationPlane();

                //TransformationPlane CurrentTP = model.GetWorkPlaneHandler().GetCurrentTransformationPlane();
                //model.GetWorkPlaneHandler().SetCurrentTransformationPlane(CurrentTP);
                //model.GetWorkPlaneHandler().SetCurrentTransformationPlane(new TransformationPlane(Beam.GetCoordinateSystem()));


                //Matrix TransformationMatrix = MatrixFactory.ByCoordinateSystems(firstWorkplane, secondWorkplane);
                //T3D.Point LocalStartPoint = TransformationMatrix.Transform(Beam.StartPoint);
                //workPlaneHandler.SetCurrentTransformationPlane(new TransformationPlane(myPart.GetCoordinateSystem()));

                //model.CommitChanges();

                #endregion test

                //// 6.
                if (myPart != null && p1 != null && p2 != null)
                {
                    // A line segment may intersect the solid surfaces in no points or more than one point
                    // So the intersection result is returned in an array list of points
                    ArrayList pointArray = myPart.GetSolid().Intersect(p1, p2);

                    // We find out how many places the line segment intersected the solid surfaces
                    //int pointCount = pointArray.Count;                                                        //Enable this to get all the points that intersect the planes
                    int pointCount = 1;

                    report = "Found " + pointCount + " point(s) of intersection";

                    // We report each point
                    for (int i = 0; i < pointCount; i++)
                    {
                        // The point must be cast as a Point in order to use it
                        var p = pointArray[i] as T3D.Point;
                        if (p != null)
                        {
                            report += "\nPoint " + (i + 1) + ": " + p;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != "Error")
                {
                    Console.WriteLine(ex.Message + ex.StackTrace);
                }
            }
            //
            return report;
        }
        //------------------------------------------------------------------------------------
        public static string SelectPointOnly()
        {
            // We build a report to the user
            string report = string.Empty;
            try
            {
                ///////////////////////////////////////////////////////////////////////////////////////
                //      Read Data from File
                ///////////////////////////////////////////////////////////////////////////////////////           
                Part myPart = null;
                //SerializeData _SerializeData = new SerializeData("SelectedPart.P2LI");
                // Retrive Deserialized Data to Part:
                TSM.Part deserializedPart = null;
                deserializedPart = _SerializeData.deserializedPart;

                // Copy to Part:
                myPart = deserializedPart;
                ///////////////////////////////////////////////////////////////////////////////////////
                //      Copied from complete code "lineIntersectionWithPart"
                //
                //      Make sure it will be fresh copied after changes

                ///////////////////////////////////////////////////////////////////////////////////////

                //// Pick Point of Intersection:
                T3D.Point p1 = PickAPoint("Pick the start of the line");

                //T3D.Point p2 = PickAPoint("Pick the end of the line");   
                double x1 = p1.X;
                double y1 = p1.Y;
                double z1 = p1.Z;

                //p1.Y += 30.0f;

                T3D.Point p2 = new T3D.Point();
                //T3D.Point p3 = new T3D.Point();
                //p3.Z = -3000.0f;

                // TODO set +/- distances for other directions (x+, x-, y+, ...):
                //...
                // Just Define p2 to be somewhere in X:
                // IF we select Y- then:    
                p2.Y = 3000.0f;

                BaseForm _BaseForm = new BaseForm();
                Class_Level_2 _Class_Level_2 = new Class_Level_2();




                //// 3. Get the currrent workplane and save it
                var model = new Model();
                var workPlaneHandler = model.GetWorkPlaneHandler();
                var firstWorkplane = workPlaneHandler.GetCurrentTransformationPlane();
                //

                //// 4. Move the Work plane to point:
                Vector Xvector = new Vector(1, 0, 0);
                Vector Yvector = new Vector(0, 1, 0);
                TransformationPlane myPlane = new TransformationPlane(p1, Xvector, Yvector);
                WorkPlaneHandler planeHandler = new Model().GetWorkPlaneHandler();
                planeHandler.SetCurrentTransformationPlane(myPlane);
                //

                //// 5. Move point reverse (Because we moved WP only. Now the point p1 will be located in the same absolute position)
                p1.X = p1.X - x1;
                p1.Y = p1.Y - y1;
                p1.Z = p1.Z - z1;

                //p1.Y += 3000.0f;

                //// 6.
                if (myPart != null && p1 != null && p2 != null)
                {
                    // A line segment may intersect the solid surfaces in no points or more than one point
                    // So the intersection result is returned in an array list of points
                    ArrayList pointArray = myPart.GetSolid().Intersect(p1, p2);

                    // We find out how many places the line segment intersected the solid surfaces
                    //int pointCount = pointArray.Count;                                                        //Enable this to get all the points that intersect the planes
                    int pointCount = 1;

                    report = "Found " + pointCount + " point(s) of intersection";

                    // We report each point
                    for (int i = 0; i < pointCount; i++)
                    {
                        // The point must be cast as a Point in order to use it
                        var p = pointArray[i] as T3D.Point;
                        if (p != null)
                        {
                            report += "\nPoint " + (i + 1) + ": " + p;
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                if (ex.Message != "Error")
                {
                    Console.WriteLine(ex.Message + ex.StackTrace);
                }
            }
            //
            return report;
        }

        //------------------------------------------------------------------------------------
        #region Other Code
        //------------------------------------------------------------------------------------
        public static void SetWorkPlaneToFace()
        {
            var model = new Model();
            var partFace = PickAFace();
            Part part = null;
            ArrayList points = null;
            foreach (InputItem inputItem in partFace)
            {
                switch (inputItem.GetInputType())
                {
                    case InputItem.InputTypeEnum.INPUT_1_OBJECT:
                        if (part == null)
                        {
                            part = inputItem.GetData() as Part;
                        }

                        break;
                    case InputItem.InputTypeEnum.INPUT_POLYGON:
                        if (points == null)
                        {
                            points = inputItem.GetData() as ArrayList;
                        }

                        break;
                }
            }

            if (part == null || points == null || points.Count < 3)
            {
                return;
            }

            var origin = points[0] as T3D.Point;
            var pointX = points[1] as T3D.Point;
            var pointY = points[2] as T3D.Point;
            if (origin == null || pointX == null || pointY == null)
            {
                return;
            }

            // PickFace returns the vertexes in the global coordinate system.
            // So we need to translate the points from global to local coordinates.
            var workPlaneHandler = model.GetWorkPlaneHandler();
            var matrixToLocal = workPlaneHandler.GetCurrentTransformationPlane().TransformationMatrixToLocal;
            origin = matrixToLocal * origin;
            pointX = matrixToLocal * pointX;
            pointY = matrixToLocal * pointY;

            var axisX = new Vector(pointX - origin);
            var axisY = new Vector(pointY - origin);
            var coordinateSystem = new CoordinateSystem(origin, axisX, axisY);
            workPlaneHandler.SetCurrentTransformationPlane(new TransformationPlane(coordinateSystem));
            model.CommitChanges();
        }
        //------------------------------------------------------------------------------------
        public static string SolidPlaneIntersection()
        {
            // We build a report to the user
            string report = string.Empty;

            TSM.Part myPart = PickAPart("Pick the part for intersecting");

            // Make pick a plane instead !!! :)
            //--
            T3D.Point p1 = PickAPoint("Pick the origin");
            T3D.Point p2 = PickAPoint("Pick a point on the plane");
            T3D.Point p3 = PickAPoint("Pick another point on the plane");
            //--

            // Set Workplane to Face
            //SetWorkPlaneToFace();
            // Get Workplane


            if (myPart != null && p1 != null && p2 != null && p3 != null)
            {
                // A line segment may intersect the solid surfaces in no points or more than one point
                // So the intersection result is returned in an array list of points
                ArrayList pointArray = myPart.GetSolid().Intersect(p1, p2, p3);

                // We find out how many places the line segment intersected the solid surfaces
                int arrayCount = pointArray.Count;

                report = "Found " + arrayCount + " array(s) of points";

                // We report each point
                for (int i = 0; i < arrayCount; i++)
                {
                    var internalPointArray = pointArray[i] as ArrayList;
                    report += "Found " + internalPointArray + " points(s) in array" + i;
                    if (internalPointArray != null)
                    {
                        for (int j = 0; j < internalPointArray.Count; j++)
                        {
                            // The point must be cast as a Point in order to use it
                            var p = internalPointArray[j] as T3D.Point;
                            if (p != null)
                            {
                                report += "\nPoint " + (j + 1) + ": " + p;
                            }
                        }
                    }
                }
            }

            return report;
        }
        #endregion

        //------------------------------------------------------------------------------------
        public static T3D.Point PickAPoint(string prompt = "Pick a Point")
        {
            T3D.Point myPoint = null;
            try
            {
                var picker = new TSMUI.Picker();
                myPoint = picker.PickPoint(prompt);
            }
            catch (Exception ex)
            {
                if (ex.Message != "User interrupt")
                {
                    Console.WriteLine(ex.Message + ex.StackTrace);
                }
            }

            return myPoint;
        }
        //------------------------------------------------------------------------------------
        public static Part PickAPart(string prompt = "Pick a part")
        {
            Part myPart = null;
            try
            {
                var picker = new TSMUI.Picker();
                myPart = picker.PickObject(TSMUI.Picker.PickObjectEnum.PICK_ONE_PART, prompt) as Part;
            }
            catch (Exception ex)
            {
                if (ex.Message != "User interrupt")
                {
                    Console.WriteLine(ex.Message + ex.StackTrace);
                }
            }

            return myPart;
        }
        //------------------------------------------------------------------------------------
        public static TSMUI.PickInput PickAFace(string prompt = "Pick a surface")
        {
            TSMUI.PickInput face = null;
            try
            {
                var picker = new TSMUI.Picker();
                face = picker.PickFace(prompt);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace);
            }

            return face;
        }
        //------------------------------------------------------------------------------------
        // Convert an object to a byte array
        public static byte[] ObjectToByteArray(System.Object obj)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }
        //------------------------------------------------------------------------------------
        // Convert a byte array to an Object
        public static System.Object ByteArrayToObject(byte[] arrBytes)
        {
            using (var memStream = new MemoryStream())
            {
                var binForm = new BinaryFormatter();
                memStream.Write(arrBytes, 0, arrBytes.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                var obj = binForm.Deserialize(memStream);
                return obj;
            }
        }
        //------------------------------------------------------------------------------------


    }
}
