﻿namespace WindowsFormsApplication2
{
    partial class BaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_start = new System.Windows.Forms.Button();
            this.btn_SelectPoint = new System.Windows.Forms.Button();
            this.btn_Y_p = new System.Windows.Forms.Button();
            this.btn_Y_m = new System.Windows.Forms.Button();
            this.btn_selectPartOnly = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(12, 12);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(75, 41);
            this.btn_start.TabIndex = 6;
            this.btn_start.Text = "Select Part and Point";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // btn_SelectPoint
            // 
            this.btn_SelectPoint.Location = new System.Drawing.Point(12, 106);
            this.btn_SelectPoint.Name = "btn_SelectPoint";
            this.btn_SelectPoint.Size = new System.Drawing.Size(75, 41);
            this.btn_SelectPoint.TabIndex = 7;
            this.btn_SelectPoint.Text = "Select Point";
            this.btn_SelectPoint.UseVisualStyleBackColor = true;
            this.btn_SelectPoint.Click += new System.EventHandler(this.btn_SelectPoint_Click);
            // 
            // btn_Y_p
            // 
            this.btn_Y_p.Location = new System.Drawing.Point(144, 12);
            this.btn_Y_p.Name = "btn_Y_p";
            this.btn_Y_p.Size = new System.Drawing.Size(45, 45);
            this.btn_Y_p.TabIndex = 8;
            this.btn_Y_p.Text = "Y+";
            this.btn_Y_p.UseVisualStyleBackColor = true;
            // 
            // btn_Y_m
            // 
            this.btn_Y_m.Location = new System.Drawing.Point(195, 12);
            this.btn_Y_m.Name = "btn_Y_m";
            this.btn_Y_m.Size = new System.Drawing.Size(45, 45);
            this.btn_Y_m.TabIndex = 9;
            this.btn_Y_m.Text = "Y-";
            this.btn_Y_m.UseVisualStyleBackColor = true;
            this.btn_Y_m.Click += new System.EventHandler(this.btn_Y_m_Click);
            // 
            // btn_selectPartOnly
            // 
            this.btn_selectPartOnly.Location = new System.Drawing.Point(12, 59);
            this.btn_selectPartOnly.Name = "btn_selectPartOnly";
            this.btn_selectPartOnly.Size = new System.Drawing.Size(75, 41);
            this.btn_selectPartOnly.TabIndex = 10;
            this.btn_selectPartOnly.Text = "Select Part";
            this.btn_selectPartOnly.UseVisualStyleBackColor = true;
            this.btn_selectPartOnly.Click += new System.EventHandler(this.btn_selectPartOnly_Click);
            // 
            // BaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 274);
            this.Controls.Add(this.btn_selectPartOnly);
            this.Controls.Add(this.btn_Y_m);
            this.Controls.Add(this.btn_Y_p);
            this.Controls.Add(this.btn_SelectPoint);
            this.Controls.Add(this.btn_start);
            this.MaximizeBox = false;
            this.Name = "BaseForm";
            this.Text = "Form1";
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Button btn_SelectPoint;
        private System.Windows.Forms.Button btn_Y_p;
        private System.Windows.Forms.Button btn_Y_m;
        private System.Windows.Forms.Button btn_selectPartOnly;
    }
}

