﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
// Tekla Structures Namespaces
using Tekla.Structures;
using Tekla.Structures.Model;
using Tekla.Structures.Geometry3d;
using Tekla.Structures.Model.UI;
using TSM = Tekla.Structures.Model;
using T3D = Tekla.Structures.Geometry3d;
using TSMUI = Tekla.Structures.Model.UI;
using TSO = Tekla.Structures.Model.Operations;
// Additional Namespace references
using System.IO;
using System.Collections;
using System.Diagnostics;
using System.Globalization;

namespace WindowsFormsApplication2
{
    public partial class BaseForm : Form
    {
        public BaseForm()
        {
            InitializeComponent();
        }
        // PUBLIC SHARED:
        public Part myPart = null;
        public string workplaneVectorDirection = "";
        public bool btn_Y_m_Clicked = false;

        //public Tuple<int, int> GetMultipleValue() { return Tuple.Create(1, 2); }

        //------------------------------------------------------------------------------------     
        private void btn_selectPartOnly_Click(object sender, EventArgs e)
        {
            string report = Class_Level_1.SelectPartOnly();
            if (report != "")
            {
                //MessageBox.Show(this, report, "Intersection points");
            }
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            string report = Class_Level_1.LineIntersectionWithPart();
            if (report != "")
            {
                MessageBox.Show(this, report, "Intersection points");
            }
        }

        private void btn_SelectPoint_Click(object sender, EventArgs e)
        {
            string report = Class_Level_1.SelectPointOnly();
            if (report != "")
            {
                MessageBox.Show(this, report, "Intersection points");
            }
        }

        private void btn_Y_m_Click(object sender, EventArgs e)
        {
            workplaneVectorDirection = "Y+";
            btn_Y_m_Clicked = true;
        }
        //------------------------------------------------------------------------------------
        /*
        private void button4_Click(object sender, EventArgs e)
        {
           string report = MyFunctions.SolidPlaneIntersection();
           if (report != "")
           {
               MessageBox.Show(this, report, "Intersection points");
           }
        }     
        private void button5_Click(object sender, EventArgs e)
        {
           MyFunctions.SetWorkPlaneToFace();
        }
        */
    }
}
